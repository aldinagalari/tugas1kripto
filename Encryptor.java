import java.util.Arrays;
import java.io.*;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher;


public class Encryptor {
  static String IV = "AAAAAAAAAAAAAAAA";
  //static String plaintext = "test text 123\0\0\0"; /*Note null padding*/
  //static String encryptionKey = "0123456789abcdef";
  public static void main(String [] args) {
    try {
      File file = new File("./tcsimple.txt");
      FileInputStream fileinput = new FileInputStream(file);
      StringBuffer strContentInput = new StringBuffer("");
      int g;
      g = fileinput.read();
      while(g != -1){
        strContentInput.append((char) g);
        g = fileinput.read();
      }
      System.out.println("==Java==");
      System.out.println("plain:   " + strContentInput);

      fileinput = new FileInputStream("keysimple.txt");

      StringBuffer strContentKey = new StringBuffer("");
      g = fileinput.read();
      while(g != -1){
        strContentKey.append((char) g);
        g = fileinput.read();
      }

      // TODO : convert String hexa to byte

      byte[] cipher = encrypt(strContentInput.toString(), strContentKey.toString());

      System.out.print("cipher:  ");
      for (int i=0; i<cipher.length; i++)
        System.out.print(new Integer(cipher[i])+" ");
      System.out.println("");

      String decrypted = decrypt(cipher, strContentKey.toString());

      System.out.println("decrypt: " + decrypted);

    } catch (Exception e) {
      e.printStackTrace();
    } 
  }

  public static byte[] encrypt(String plainText, String encryptionKey) throws Exception {
    Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5Padding");

    // TODO : cek untuk 3 kemungkinan key menggunakan 128/256/192 bit
    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes(), "AES");
    cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes()));


    return cipher.doFinal(plainText.getBytes());
  }

  public static String decrypt(byte[] cipherText, String encryptionKey) throws Exception{
    Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5Padding");

    // TODO : cek untuk 3 kemungkinan key menggunakan 128/256/192 bit
    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes(), "AES");
    cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes()));


    return new String(cipher.doFinal(cipherText));
  }
}